terraform {
  required_providers {
    google = "3.51.0"
  }
  required_version = "0.13.5"
}

provider "google" {
  project = var.project
  region  = var.region
  zone = var.zone
}
